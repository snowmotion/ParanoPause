#=========================================================================================================
# PARANOPAUSE V1.1
# Date: 01.12.2016
# Goal: This script is made to be executed at every break time of the user. It will 
# make a backup of the specified folder and prompt the user if he wants to proceed working.
# if no answer is given, the station is locked after a certain timeout.
# This script is made of two parts: A configuration form and an Execution mode
# If launched without arguments, the Configuration Window will open
# Configuration: 
#   in the configuration, you can configure source/destination folders, test the configuration and save it
#   you can also add and remove an execution time, which will automatically be set in the task schedulers
#=========================================================================================================
Param(#Script Parameters
    [switch]$execution,
    [switch]$Alarm
)
#IMPORTS==================================================================================================

Add-Type -AssemblyName 'System.Drawing', 'System.Windows.Forms'
Add-Type -Name Window -Namespace Console -MemberDefinition '
[DllImport("Kernel32.dll")]
public static extern IntPtr GetConsoleWindow();

[DllImport("User32.dll")]
public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);
'
#function to hide the console Window(or show it again)
function Hide-Console([Switch]$show){
    $consolePtr = [Console.Window]::GetConsoleWindow()
    if($show){
        [Console.Window]::ShowWindow($consolePtr, 5)
    } else {
        [Console.Window]::ShowWindow($consolePtr, 0)
    }
}

#CONSTANTS AND VARIABLES==================================================================================
$TimestampFormat = "%Y-%m-%d@%H-%M"
$BalloonIconType = "Info"
$BalloonTitle = "ParanoPause"

#Do not change these
New-Variable -Name TaskName -Value "ParanoPause Execution" -Option Constant -ErrorAction SilentlyContinue
New-Variable -Name SaveDir -Value "$env:APPDATA\ParanoPause" -Option Constant -ErrorAction SilentlyContinue -Scope Script
New-Variable -Name SaveFile -Value "config.xml" -Option Constant -ErrorAction SilentlyContinue -Scope Script
New-Variable -Name DefaultTimeOut -Value 1 -Option Constant -ErrorAction SilentlyContinue

#Function to import Configuration Values
Function ImportConf{
    if (!(Test-Path -Path "$SaveDir")){
        Mkdir $SaveDir | Out-Null
    }
    if ((Test-Path -Path "$SaveDir\$SaveFile")){
        $config = Import-Clixml $SaveDir"\"$SaveFile
    } else {
        $config = New-Object PSObject -Property @{source = ""; dest=""; recurse=""; timeTillLockMin=$DefaultTimeOut;
         times = New-Object Collections.ArrayList;
        }
    }
    return $config
}

$config = ImportConf

#ICON=====================================================================================================
$bmp = New-Object Drawing.Bitmap(16,16)
$g = [Drawing.Graphics]::FromImage($bmp).FillRectangle(
    (New-Object Drawing.Drawing2D.LinearGradientBrush(
        (New-Object Drawing.Point(0,0)),
        (New-Object Drawing.Point(16, 16)),
        [Drawing.Color]::FromArgb(0, 0, 255, 0),
        [Drawing.Color]::FromArgb(255, 255, 255, 0))),
    0, 0, $bmp.Width, $bmp.Height)

$ico = [Drawing.Icon]::FromHandle($bmp.GetHicon())

#EXEC PART OF THE SCRIPT================================================================================
Function execute($config){
    
    Function global:Lock{
        $Rundll32 = "$env:windir\System32\rundll32.exe"
        Start-Process -FilePath $Rundll32 -ArgumentList "user32.dll, LockWorkStation"
    }
    
    #Function to backup the files
    Function Backup{
        if(($config.source -and $config.dest) -ne $null){
            $Timestamp = Get-Date -UFormat $TimestampFormat
            if ($config.recurse){
            robocopy "$Source" "$Dest\$Timestamp" /E | Out-Null
            } else {
            robocopy "$Source" "$Dest\$Timestamp" | Out-Null
            }
        }
        #Function to show the user Information about the backup
        Function ShowInfo (){
           if(($config.source -ne "")-and ($config.dest -ne ""))
            {
                [Windows.Forms.MessageBox]::Show("Files copied from $($config.source) to $($config.dest)","ParanoPause") | Out-Null
            }
        }
        ShowInfo
    }
    
    $global:TimeTillLock = $config.TimeTillLockMin*60
    $global:IntervalCounter = 0
    $global:waitCounter = $true
    $global:timeLeft = $TimeTillLock
    
    $script:TrayIcon = New-Object Windows.Forms.NotifyIcon
    $TrayIcon.Icon = $ico
    $TrayIcon.Visible = $true
    $ChoiceWinCount = 0
    
    $ClickTray = {
        if($ChoiceWinCount -eq 0){
    	    $ChoiceWinCount++
            Function ChoiceLock(){
                $Message = "PC is going to be locked in $global:timeLeft Seconds. Lock the PC? `
Yes: lock now `r`nNo: prevent lock"
                $choice = [Windows.Forms.MessageBox]::Show($Message, $BalloonTitle, 3, "Question")
                if ($choice -eq "Yes"){
                    $global:waitCounter=$false
                    Lock
                } elseif ($choice -eq "No"){
                    $global:waitCounter = $false
                }
            }
            ChoiceLock
            $ChoiceWinCount = 0
        }
    }
    
    $TrayIcon.add_Click($ClickTray)
    
    $Interval = {
        $global:IntervalCounter++
        $global:timeLeft = $global:TimeTillLock-$global:IntervalCounter
        if($global:IntervalCounter -eq $global:TimeTillLock-15){
            $BalloonText = "Session is going to be locked in $global:Timeleft Seconds"
            $TrayIcon.ShowBalloonTip(5,$BalloonTitle, $BalloonText, $BalloonIconType)
        } elseif ($global:IntervalCounter -eq $global:TimeTillLock){
            Lock
            $global:waitCounter = $false                
        }
    }
    
    $global:Timer = New-Object System.Timers.Timer(1000)    Unregister-Event -SourceIdentifier TimerEvent -ea SilentlyContinue    Register-ObjectEvent $Timer -EventName Elapsed `    -SourceIdentifier TimerEvent -Action $Interval | Out-Null
    $Timer.Start()
    
    $TrayIcon.Visible = $true
    $BalloonText = "Session is going to be locked in $TimeTillLock Seconds"
    $TrayIcon.ShowBalloonTip(5,$BalloonTitle, $BalloonText, $BalloonIconType)
    
    #Wait for the timer to finish
    while($global:waitCounter){
        #$global:waitCounter = $Timer.Enabled
        start-sleep -Milliseconds 100
    }
    $Timer.Stop()
    $TrayIcon.Visible = $false
    $TrayIcon.Dispose()
    Backup
    Unregister-Event -SourceIdentifier TimerEvent -ea SilentlyContinue
}
#Configuration Form=======================================================================================
Function GenerateForm($config){
    
    #Function to Create the task
    Function CreateTask(){    
        $ScriptPath = $MyInvocation.PSCommandPath
        #Define the task action
        $Action = New-ScheduledTaskAction -Execute "Powershell.exe" `
        -Argument "-ExecutionPolicy Bypass -NoProfile -File `"$ScriptPath`" -RunType True -Execution"
        #Define the triggers of the task
        $Triggers = @()
        foreach($time in $config.times){
    		
            $Triggers += New-ScheduledTaskTrigger -Daily -At $time
        }
        $Description = "Paranopause will save your files based on its configuration `
        (configuration: .\Paranopause.ps1) and then lock your session"
        #Register Task
        Register-ScheduledTask -TaskName $TaskName -Action $Action `
        -Trigger $Triggers -Description $Description | Out-Null
    }
    
    #Function to set the execution times of the script
    Function SetExecTimes(){
        $ScriptPath = $MyInvocation.PSCommandPath
        $Triggers = @()
        #Define the task action
        $Action = New-ScheduledTaskAction -Execute "Powershell.exe" `
        -Argument "-ExecutionPolicy Bypass -NoProfile -File `"$ScriptPath`" -RunType True -Execution"
        foreach($Time in $config.times){
    		$TimeObj =  Get-Date -Date $Time
            $TimeCalc = $TimeObj.AddMinutes(-$config.timeTillLockMin)
            $Triggers += New-ScheduledTaskTrigger -Daily -At $TimeCalc
        }
        Set-ScheduledTask -TaskName "$TaskName" -Trigger $Triggers -Action $Action
    }
    
    #Function to set the scheduled task with the triggers
    Function UpdateTask(){
        #get the task
        Try {
            $Task = Get-ScheduledTask -TaskName "$TaskName" -ErrorAction Stop
        }
        Catch{
            $Task = $Null
        }
        #if the scheduled task exists and the time list isnt empty, update the triggers
        if (($Task -ne $Null) -and ($config.times.Count -ne 0)){
            SetExecTimes
        }
        #if the time list is empty
        elseif ($config.Times.Count -eq 0){
            #if the task exists, delete it
            if ($Task -ne $Null){
                Unregister-ScheduledTask -TaskName "$TaskName" -Confirm:$false
            }
        } else {#else create the task with the triggers
            CreateTask
        }
    }
    
    #Function to put the times in the listBreaks
    Function setBreaksList(){
        $ListBreaks.Items.Clear()
        foreach($Time in $config.times){
            $ListBreaks.Items.Add($Time)
        }
    }
    
    #Function to tell the user that a path is invalid
    Function InvalidPath($path){
        if (($path -ne $null) -and ($path -ne "")){
            if (!(Test-Path $path)){
                [Windows.Forms.MessageBox]::show("`"$path`" is not a valid directory", "Invalid directory", 0, "Error")
            }
        }
    }
    
    #Function to restore the values in the Form
    Function RestoreForm{
        $BoxRecurse.Checked = $config.recurse
        $TextSource.Text = $config.source
        $TextDest.Text = $config.dest
        if ($config.times -ne $null){
            ForEach ($Time in $config.times){
                $ListBreaks.Items.Add($Time) | Out-Null
            }
        }
        $TimeOut.Value = $config.timeTillLockMin
    }
    
    #Function to select a Folder with the Explorer
    Function Get-Folder([string]$Text, $Start = "p:\"){
        $FolderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog -Property @{
	        selectedpath = $Start
        }
        $FolderBrowser.ShowDialog() | Out-Null
        return $FolderBrowser.SelectedPath
    }
    
    #Function to Export the values in the Configuration Form
    Function ExportConf{
        $config | Export-Clixml "$SaveDir\$SaveFile"
    }
    
    #Function to save the times list, refresh the combobox and update the task
    Function UpdateTimes($NewTimes){
        #refresh the combobox
        setBreaksList $NewTimes
        #Update Task
        UpdateTask $NewTimes
    }
    
    #Beginning of the actual Form code---------------------------------------------------------
    
    #region Generated Form Objects
    $ParanoPauseG = New-Object Windows.Forms.Form
    $LabelBackup = New-Object Windows.Forms.Label
    $BtnSource = New-Object Windows.Forms.Button
    $TextSource = New-Object Windows.Forms.TextBox
    $BoxRecurse = New-Object Windows.Forms.CheckBox
    $BtnDest = New-Object Windows.Forms.Button
    $TextDest = New-Object Windows.Forms.TextBox
    $LabelManage = New-Object Windows.Forms.Label
    $TimePicker = New-Object Windows.Forms.DateTimePicker
    $BtnAddBreak = New-Object Windows.Forms.Button
    $ListBreaks = New-Object Windows.Forms.ComboBox
    $BtnRemoveBreak = New-Object Windows.Forms.Button
    $LabelTimeout = New-Object Windows.Forms.Label
    $TimeOut = New-Object System.Windows.Forms.NumericUpDown
    $BtnTest = New-Object Windows.Forms.Button
    
    RestoreForm #Set the values for the form
    
    #Actions for the form elements------------------------------------------------------------
    $BtnSource_OnClick = {
        $TextSource.Text = Get-Folder "Select Backup Source Directory" $config.source
        $config.Source = $TextSource.Text
        InvalidPath $config.source
    }
    
    $TextSource_Leave = {
        $config.Source = $TextSource.Text
        InvalidPath $config.source
    }
        
    $BtnDest_OnClick = {
        $TextDest.Text = Get-Folder "Select Backup Destination Folder" $config.dest
        $config.Dest = $TextDest.Text
        InvalidPath $config.dest
    }

    $TextDest_Leave = {
        $config.Dest = $TextDest.Text
        InvalidPath $config.dest
    }
    
    $BoxRecurse_CheckedChanged = {
        if($BoxRecurse.Checked){
            $config.recurse = $true
        } else {
            $config.recurse = $false
        }
    }

    $BtnTest_OnClick = {
        ExportConf
        Execute $config
    }
    
    $BtnRemoveBreak_OnClick = {
        #Get the selected time
        $Sel = $ListBreaks.SelectedItem
        if($Sel -ne $null){
            $SelTime = $Sel.ToString()
            #Remove it from stored break times list    
            foreach ($Time in $config.Times){
                if($Time -eq $SelTime){
                    $config.times.Remove($SelTime)
                    break
                }
            }
            UpdateTimes $config.times
        } 
    }
    
    $BtnAddBreak_OnClick = {
        if ($config.times.Count -lt 10){
            $TimeStr = $TimePicker.Value.ToString("HH:mm:ss")
            $config.times.Add($TimeStr)         
            UpdateTimes $Times
        } else {
            [Windows.Forms.MessageBox]::show("You can't have more breaks", "Too many breaks", 0, "Error")
        }
    }
    
    $TimeOutChanged = {
        $config.TimeTillLockMin = $TimeOut.Value
        SetExecTimes $config.Times
    }
    
    #Configuration form elements--------------------------------------------------------------
    #Backup-------------------------------------------------------------------------
    
    #Title
    $LblTitleFont = New-Object Drawing.Font("Arial", 12, 1, 3)

    #General
    $TxtFont = New-Object Drawing.Font("Arial", 9.5, 0, 3)

    #TextBox
    $TboSize = New-Object Drawing.Size(520)
    $TboFont = New-Object Drawing.Font("Arial", 10, 0, 3)

    #Label
    $LblFont = New-Object Drawing.Font("Arial", 11, 0, 3)
    
    #ConfigurationForm
    $ParanoPauseG.ClientSize = New-Object Drawing.Size(600,400)
    $ParanoPauseG.Text = "ParanoPause"
    $ParanoPauseG.StartPosition = "CenterScreen"
    
    #LabelManageBackup
    $LabelBackup.Font = $LblTitleFont
    $LabelBackup.TextAlign = 2
    $LabelBackup.Location = New-Object Drawing.Point(30,20)
    $LabelBackup.Size = New-Object Drawing.Size(500,30)
    $LabelBackup.Text = "Manage your backup"

    #BtnBackupSource
    $BtnSource.Location = New-Object Drawing.Point(30,60)
    $BtnSource.Size = New-Object Drawing.Size(110,27)
    $BtnSource.Text = "Source"
    $BtnSource.Font = $TxtFont
    $BtnSource.add_Click($BtnSource_OnClick)
    
    #BoxBackupRecurse
    $BoxRecurse.Location = New-Object Drawing.Point(150,60)
    $BoxRecurse.Size = New-Object Drawing.Size(200,24)
    $BoxRecurse.Text = "Include Subfolders"
    $BoxRecurse.Font = $TxtFont
    $BoxRecurse.add_CheckedChanged($BoxRecurse_CheckedChanged)
    
    #TextBackupSource
    $TextSource.Location = New-Object Drawing.Point(30,90)
    $TextSource.Size = $TboSize
    $TextSource.Font = $TboFont
    $TextSource.add_Leave($TextSource_Leave)
    
    #BtnDest
    $BtnDest.Location = New-Object Drawing.Point(30,120)
    $BtnDest.Size = New-Object Drawing.Size(110,27)
    $BtnDest.Text = "Destination"
    $BtnDest.Font = $TxtFont
    $BtnDest.add_Click($BtnDest_OnClick)
    
    #TextBackupDestination
    $TextDest.Location = New-Object Drawing.Point(30,150)
    $TextDest.Size = $TboSize
    $TextDest.Font = $TboFont
    $TextDest.add_Leave($TextDest_Leave)
    
    #Break-Times--------------------------------------------------------------------
    
    #LabelManageBreaks
    $LabelManage.Font = $LblTitleFont
    $LabelManage.TextAlign = 2
    $LabelManage.Location = New-Object Drawing.Point(30,200)
    $LabelManage.Size = New-Object Drawing.Size(500,30)
    $LabelManage.Text = "Manage your break-times"
    
    #TimepickerNewBreak
    $TimePicker.Location = new-object Drawing.Point(30, 250)
    $TimePicker.Size = new-object Drawing.Size(82, 27)
    $TimePicker.Format = [Windows.Forms.DateTimePickerFormat]::Time
    $TimePicker.ShowUpDown = $True
    $TimePicker.Font = $TboFont
    
    #BtnAddBreak
    $BtnAddBreak.Location = New-Object Drawing.Point(120,250)
    $BtnAddBreak.Size = New-Object Drawing.Size(100,27)
    $BtnAddBreak.Text = "Add"
    $BtnAddBreak.Font = $TxtFont
    $BtnAddBreak.add_Click($BtnAddBreak_OnClick)
    
    #DropdownBreaks
    $ListBreaks.DropDownStyle = 2
    $ListBreaks.Location = New-Object Drawing.Point(280,250)
    $ListBreaks.Size = New-Object Drawing.Size(150,27)
    $ListBreaks.Font = $TboFont
    
    #BtnRemoveBreak
    $BtnRemoveBreak.Location = New-Object Drawing.Point(450,250)
    $BtnRemoveBreak.Size = New-Object Drawing.Size(100,27)
    $BtnRemoveBreak.Text = "Remove"
    $BtnRemoveBreak.add_Click($BtnRemoveBreak_OnClick)
    $BtnRemoveBreak.Font = $TxtFont
    
    #Label modify TimeOut
    $LabelTimeout.TextAlign = 1
    $LabelTimeout.Location = New-Object Drawing.Point(30,300)
    $LabelTimeout.Size = New-Object Drawing.Size(240,20)
    $LabelTimeout.Text = "Timeout before breaks (minutes):"
    $LabelTimeout.Font = $LblFont

    #NumericTimeOut
    $TimeOut.Location = New-Object System.Drawing.Point (280,300)
    $TimeOut.Size = New-Object System.Drawing.Size (40,20)
    $TimeOut.Minimum = 1
    $TimeOut.Maximum = 15
    $TimeOut.Increment = 1
    $TimeOut.DecimalPlaces = 0
    $TimeOut.ReadOnly = $false
    $TimeOut.add_ValueChanged($TimeOutChanged)
    $timeout.Font = $TboFont
    
    #BtnTest
    $BtnTest.Location = New-Object Drawing.Point(450,325)
    $BtnTest.Size = New-Object Drawing.Size(100,27)
    $BtnTest.Text = "Test"
    $BtnTest.add_Click($BtnTest_OnClick)
    $BtnTest.Font = $TxtFont

    #TabIndex
    $BtnSource.TabIndex = 0
    $BoxRecurse.TabIndex = 1
    $TextSource.TabIndex = 2
    $BtnDest.TabIndex = 3
    $TextDest.TabIndex = 4
    $TimePicker.TabIndex = 8
    $BtnAddBreak.TabIndex = 10
    $ListBreaks.TabIndex = 12
    $BtnRemoveBreak.TabIndex = 14
    $TimeOut.TabIndex = 16
    $BtnTest.TabIndex = 18
    
    #Diverse things to do with the form (icon, maximizing)---------------------------------
    #Add elements to the configuration form
    $ParanoPauseG.Controls.AddRange(@($TimePicker,$BtnSource,$TextSource,$BtnDest,$TextDest,$BoxRecurse,`
    $BtnTest,$ListBreaks,$LabelManage,$BtnRemoveBreak,$BtnAddBreak,$LabelTimeout,$TimeOut, $LabelBackup))
    $ParanoPauseG.FormBorderStyle = 'Fixed3D'
    $ParanoPauseG.MaximizeBox = $false
    $ParanoPauseG.Icon = $ico
    $ParanoPauseG.add_FormClosing({
        ExportConf
    })
    #Use the Windows Aero Visual Style
    [Windows.Forms.Application]::EnableVisualStyles()
    $ParanoPauseG.ShowDialog()| Out-Null
} #End Configuration Form

#Program Start============================================================================================
#Hide the console
if($Host.Name -eq "ConsoleHost"){
    Hide-Console | Out-Null
}
#Select the correct execution mode
if($execution){   
    Execute $config
    Write-Eventlog -Logname 'Windows PowerShell' -source Powershell -eventID 600 -EntryType Information �Message �ParanoPause launched in Execution mode�
    }
else {
    GenerateForm $config
}
#Show the console again (if the console called the script)
if($Host.Name -eq "ConsoleHost"){
    Hide-Console -show | Out-Null
}