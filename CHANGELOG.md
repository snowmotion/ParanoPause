# Changelog  
## 1.2  
### RC4
* Updated function updateTask to simplify code
* sorting timelist
* If timeout is changed, the task is only updated on form closing to improve performance

### RC3  
* Function lock is now global to prevent a bug  
* Timer end test changed to be "equal to" the total time instead of "greater equal"  to prevent an endless loop  

### RC4
* Powershell Window now doesn't appear at execution'
* Fixed various bugs
* Simplified code

### RC5
* Error message if the backup is not successful

### RC6
* Update location at each start
